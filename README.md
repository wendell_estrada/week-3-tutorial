# 1. Audit Log
- Create a tab panel with an empty container.
- Set the "createAlias" property of the container to "auditlogtree".
- Set the "enableAudit" property in the view controller's setupContext method to true.
---
# 2. Activities
- Create a tab panel with an empty container.
- Set the "createAlias" property of the container to "activitygrid".
----
# 3. Attachments
- Create a tab panel with an empty container.
- Set the "createAlias" property of the container to "attachmentgrid".
---
# 4. Master/Details
- Enable navigation properties in Web API.
 
##### MODELS:
 
```java
public class tblICItem {
    public int intItemId { get; set; }
    
    public ICollection<tblICItemUOM> tblICItemUOM { get; set; }
    public ICollection<tblICItemLocation> tblICItemLocation { get; set; }
}

public class tblICItemUOM {
    public int intItemUOMId { get; set; }
    public tblICItem tblICItem { get; set; }
}

public class tblICItemLocation {
    public int intItemLocationId { get; set; }
    public tblICItem tblICItem { get; set; }
}
```
 
#### Controller/Business Layer:
- Override existing controller actions or create new ones:
```java 
public async Task<GetObjectResult> GetItemUOMs(GetParameter param)
{
    var query = _db.GetQuery<tblICItemUOM>().Filter(param);
    return new GetResult<tblICItemUOM>() {
        data = await query.AsNoTracking().ToListAsync(),
        total = await query.CountAsync()
    };
}
```
- Set up master/details grid in view controller's setupContext method. (see sample #4)
- Set up stores
```javascript
{
    name: 'intUnitMeasureId',
    type: 'int',
    reference: {
        type: 'Inventory.model.UnitMeasure',
        inverse: {
            role: 'tblUnitMeasure',
            storeConfig: {
                complete: true, // This is important
                remoteFilter: true,
                proxy: {
                    type: 'rest',
                    api: {
                        read: '../inventory/api/item/getitemuoms'
                    },
                    reader: {
                        type: 'json',
                        rootProperty: 'data'
                    }
                }
            }
        }
    }
},
```

---
# 5. Dynamic Filters
- Used to filter items in a grid combobox dynamically. See sample #5 above.

#### Commonly-used filter conditions
     
Keyword|Description
-------|-----------
gt|Greater than 
lt|Less than
eq|Equals
noteq|Not Equals
blk|Blank
bw|Between
ct|Contains
  
#### Conjunctions
      
Keyword|Description
---|---
and|all conditions must be true
or|one of the conditions must be true

---

# 6. Formula Bindings
- See sample #6.

---

# 7. Tables & Stores

Item Name | Table | Alias | Store
----------|-------|-------|------
Item|tblICItem|Inventory.store.Item|icitem
Item Location|tblICItemLocation|Inventory.store.ItemLocation|icitemlocation
Item UOM (Unit of Measure)|tblICItemUOM|Inventory.store.ItemUOM|icitemuom     
    
#### LOOKUP TABLES    

Item Name | Table | Alias | Store
----------|-------|-------|------
Unit of Measure|tblICUnitMeasure|Inventory.store.UnitMeasure|icunitmeasure
Manufacturer|tblICManufacturer|Inventory.store.BufferedManufacturer|icbufferedmanufacturer
Category|tblICCategory|Inventory.store.BufferedCategory|icbufferedcategory
Commodity|tblICCommodity|Inventory.store.BufferedCommodity|icbufferedcommodity
  
### Framework References: http://inet.irelyserver.com/display/ID/Performance+Guidelines

### Demo Source Code

```javascript
Ext.define('Inventory.view.ItemViewController', {
    setupContext: function(win) {
        var me = this,
            win = options.window,
            store = Ext.create('Inventory.store.Item', { pageSize: 1 }),
            grdItemLocation = win.down("#grdItemLocation"),
            grdUnitMeasure = win.down("#grdUnitMeasure");

        win.context = Ext.create('iRely.Engine', {
            window  : win,
            store   : store,
            binding : me.config.binding,
            // 1. Audit Log
            enableAudit: true, 
            // 2. Activities
            enableActivity: true,
            createTransaction: Ext.bind(me.createTransaction, me),
            // 3. Attachments
            attachment: Ext.create('iRely.attachment.Manager', {
                type: 'Inventory.Item',
                window: win
            }),
            // 4. Master-Details
            details : 
            [
                {
                    key: 'tblICUnitMeasure',
                    lazy: true,
                    component: Ext.create('iRely.grid.Manager', {
                        grid: grdUnitMeasure
                    }),
                },
                {
                    key: 'tblICItemLocation',
                    lazy: true,
                    component: Ext.create('iRely.grid.Manager', {
                        grid: grdItemLocation
                    }),
                },
            ]
        });
    },

    config: {
        binding: {
            // 5. Dynamic Filters
            txtItemNo: {
                value: '{getItemName}'
            },
            cboCommodity: {
                value: '{current.strCommodityCode}',
                origValueField: 'intId',
                origUpdateField: 'intCommodityId',
                store: '{commodity}',
                readOnly: '{isCommodityReadOnly}',
                defaultFilters: [
                    {
                        column: 'strInventoryType',
                        value: '{current.strType}',
                        condition: 'eq'
                    },
                    {
                        column: 'ysnActive',
                        value: true,
                        condition: 'eq',
                        conjunction: 'and'   
                    },
                    // Grouped Filters
                    {
                        inner: [
                            {
                                column: 'intAnotherField',
                                value: '{current.anotherValue}',
                                condition: 'eq',
                                conjunction: 'and'
                            },
                            {
                                column: 'intAnotherField2',
                                value: '{current.anotherValue2}',
                                condition: 'noteq',
                                conjunction: 'and'
                            }
                        ],
                        conjunction: 'or'
                    }
                ]
            }
        }
    }
});

Ext.define('Inventory.view.ItemViewModel', {
    data: {
        intCurrentId: 0
    },

    // 6. Formulas/Formula Bindings
    formulas: {
        // Set commodity dropdown to read only when the item is not active.
        isCommodityReadOnly: function(get) {
            return !get('current.ysnActive');
        },

        getItemName: function(get) {
            return get('current.ysnActive') ? get('current.strItemNo') : get('current.strItemNo').concat(' (Inactive)');
        }
    }
});
```